# OhKBIC starter code

[Competition website](http://biometric-competitions.com/mod/competition/view.php?id=7)

Use this code to get started with the One-handed Keystroke Biometric Identification Competition, an official competition of the 8th IAPR International Conference on Biometrics.

This code computes some commonly-used keystroke biometric features, such as key durations and latencies. However, it performs very poorly.

##TODO
Changes that could be made to see immediate improvements are:

### A fallback mechanism for missing features. 
See a paper (http://scholar.google.com/citations?view_op=view_citation&citation_for_view=iG64YJUAAAAJ:uWQEDVKXjbEC) on missing keystroke data for more information.
### Better features. 
There are only a couple dozen features used here. Try to figure out which ones work well, or write an algorithm to do that. There are many feature selection algorithms already out there.
### More sophisticated machine learning algorithm
A KNN classifier might not be the best choice here, especially with one sample in the training dataset. The samples could be partitioned to create more training samples, but this leads to more missing features in each sample.