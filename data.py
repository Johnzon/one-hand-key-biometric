'''
Created on Jul 29, 2014

@author: vinnie
'''

import os
import numpy as np
import pandas as pd
from IPython import embed

PROJECT_DIR = ''
DATA_DIR = ''


def load(dataset):
    df = pd.read_csv(os.path.join(DATA_DIR, '{}.csv'.format(dataset)))
    df['condition'] = dataset
    return df

def keystrokes_to_text(df):
    return ''.join(filter(lambda x: len(x)==1, df['keyname'].replace('period','.').replace('space',' ')))

def compare_sizes():

    both = load('both')
    left = load('left')
    right = load('right')

    both_text = both.groupby(level=0).apply(keystrokes_to_text)
    left_text = right.groupby(level=0).apply(keystrokes_to_text)
    right_text = right.groupby(level=0).apply(keystrokes_to_text)

    size = pd.concat([both.groupby(level=0).size(),
                      left.groupby(level=0).size(),
                      right.groupby(level=0).size()], axis=1)
    size.columns = ['both','left','right']

    return size

def create_samples(df, s, pad):
    session = np.arange(len(df))//(s+pad)
    df['sample'] = session
    df = df.groupby('sample').apply(lambda x: x[:s])
    df = df.groupby('sample').filter(lambda x: len(x)==s)
    return df

def anonymize(df, user_map):
    df['user'] = df['email'].map(user_map)
    return df

def normalize(df):
    
    df['timerelease'] -= df.iloc[0]['timepress']
    df['timepress'] -= df.iloc[0]['timepress']
    
    return df

def renumber_samples(df):
    df = df.reset_index(drop=True) # get rid of other indices
    df['sample'] = ((df['session'].shift() != df['session']).astype(bool) | df['sample'].diff().fillna(0).astype(bool)).cumsum()
    return df

def create_datasets(s=500, pad=50):

    both = load('both')
    left = load('left')
    right = load('right')

    # Create samples with padding between samples
    both = both.groupby(['email','session']).apply(lambda x: create_samples(x, s, pad))
    left = left.groupby(['email','session']).apply(lambda x: create_samples(x, s, pad))
    right = right.groupby(['email','session']).apply(lambda x: create_samples(x, s, pad))
    
    # Renumber the sessions starting at 0
    both = both.groupby(level=0).apply(renumber_samples).reset_index(level=1, drop=True)#.set_index('sample', append=True)
    left = left.groupby(level=0).apply(renumber_samples).reset_index(level=1, drop=True)#.set_index('sample', append=True)
    right = right.groupby(level=0).apply(renumber_samples).reset_index(level=1, drop=True)#.set_index('sample', append=True)

    # Assign a random number to each user
    users = both['email'].unique()
    np.random.shuffle(users)
    user_map = {u:i for i,u in enumerate(users)}

    # Anonymize the user labels
    both = anonymize(both, user_map)
    left = anonymize(left, user_map)
    right = anonymize(right, user_map)
    
    # First both sample is the training set
    train = both[both['sample'] == 1]
    train = train[['user','condition','handedness','typingstyle','timepress','timerelease','keyname']]
    train.sort(['user','timepress'], inplace=True)
    train = train.set_index(['user'])
    train = train.groupby(level=0).apply(normalize)
    train.to_csv(os.path.join(DATA_DIR, 'train.csv'))

    # Keep only users that appear in the training set
    train_users = set(train.index.unique())
    left = left[left['user'].isin(train_users)]
    right = right[right['user'].isin(train_users)]
    
    # Rest of both and left/right are testing
    both_test = both[both['sample'] > 1]
    test = pd.concat([both_test, left, right])
    
    # Create a UUID for each sample, unique by user, condition, sample (from above)
    s = pd.DataFrame(test.groupby(['user','condition','sample']).size(), columns=['sampleUUID'])
    s['sampleUUID'] = np.random.permutation(np.arange(len(s)))
    test = test.join(s, on=['user','condition','sample'])

    # Dataframe for the correct user labels for each sample UUID
    correct = s.reset_index().set_index('sampleUUID')[['user','condition']]
    correct.columns = ['user','dataset']
    correct.sort_index(inplace=True)
    correct.index.name = 'sample'
    correct.to_csv(os.path.join(DATA_DIR, 'correct.csv'))

    # Finally, the test dataset
    test.sort(['sampleUUID','timepress'], inplace=True)
    test = test.set_index('sampleUUID')
    test.index.name = 'sample'
    test_ext = test[['condition','handedness','typingstyle','timepress','timerelease','keyname']]
    test = test[['condition','timepress','timerelease','keyname']]
    
    test = test.groupby(level=0).apply(normalize)
    test.to_csv(os.path.join(DATA_DIR, 'test.csv'))
    test_ext.to_csv(os.path.join(DATA_DIR, 'test_ext.csv'))
    return

if __name__ == '__main__':
    create_datasets()