'''
Created on Aug 30, 2014

@author: vinnie
'''
import sys
import string
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier

# List of key names for duration features

duration_keys = list(string.ascii_lowercase) + ['shift','space','backspace','period']
# List of tuples for key transition features
transition_key_pairs = [
('e','space'),
('space', 'e'),
('i','space'),
('t','space'),
]

def chunks(l, n):
    '''
    Yield n successive chunks from l.
    '''
    newn = int(len(l) / n)
    for i in range(0, n-1):
        yield l[i*newn:i*newn+newn]
    yield l[n*newn-newn:]

def remove_outliers(df, distance=2):
    '''
    Remove outliers from a series of observations.
    Anything farther than 3xstd is removed.
    '''
    m = df.mean()
    s = df.std()
    return df[(df>(m - distance*s)) & (df<(m + distance*s))]

def durations(df, keys=duration_keys):
    '''
    Get the mean and standard deviation of keystroke durations for a list
    of key names. Outliers are removed
    '''
    # Duration is the time the key is held down for
    df['duration'] = df['timerelease'] - df['timepress']
    
    # Index by key name and remove outliers
    d = remove_outliers(df.set_index('keyname')['duration'])
    
    # Mean and standard deviation of each key duration will make up the features
    m = d.groupby(level=0).mean()
    s = d.groupby(level=0).std()
    
    # Select only the keys in our list of features
    return pd.concat([m.loc[keys], s.loc[keys]]).fillna(0)

def transitions(df, key_pairs=transition_key_pairs):
    '''
    Get the press-press and release-press mean transition times for
    a list of key-pair tuples. Outliers are removed.
    '''
    # Stagger the keystrokes to get consecutive keystrokes in each row
    t = pd.concat([df.shift(), df], axis=1).dropna()
    t.columns = ['{}_1'.format(c) for c in df.columns] +  ['{}_2'.format(c) for c in df.columns]
    
    # Index by a tuple of the key names
    t = t.set_index(t[['keyname_1', 'keyname_2']].to_records(index=False))
    
    # Press-press and release-press latencies
    t['pp'] = t['timepress_2'] - t['timepress_1']
    t['rp'] = t['timepress_2'] - t['timerelease_1']
    
    # Mean of each type of latency after outlier removal
    pp = remove_outliers(t['pp']).groupby(level=0).mean()
    rp = remove_outliers(t['rp']).groupby(level=0).mean()
    
    # Select only the key pairs in our list of features
    return pd.concat([pp.loc[key_pairs], rp.loc[key_pairs]]).fillna(0)

def keystroke_features(df):
    '''
    Get duration and transition keystroke features from a series of keystrokes
    '''
    return pd.concat([durations(df), transitions(df)])

def chunked_keystroke_features(df, n=1):
    '''
    First, break a series of keystrokes into n chunks. Then, get duration
    and transition keystroke features from each chunk.
    '''
    return pd.concat([keystroke_features(x) for x in chunks(df, n)], axis=1).T

def normalize(train, test):
    '''
    Normalize training and testing feature vectors.
    Test features are normalized using the bounds of the train features
    '''
    upper = train.max().copy()
    lower = train.min().copy()
    train = (train - lower)/(upper - lower)
    test = (test - lower)/(upper - lower)
    test[test < 0] = 0
    test[test > 1] = 1
    return train.fillna(0), test.fillna(0)

if __name__ == '__main__':
    if len(sys.argv) != 4:
        sys.exit('Usage:\n$ python benchmark.py [train-file] [test-file] [submission-file]')
        
    fname_train = sys.argv[1]
    fname_test = sys.argv[2]
    fname_result = sys.argv[3]

    # Load the train and test CSV files as pandas dataframes, indexed by the user and sample columns
    train = pd.read_csv(fname_train, index_col=['user'])
    test = pd.read_csv(fname_test, index_col=['sample'])
    
    # Training samples can be chunked for each user to obtain several feature vectors
    train = train.groupby(level=0).apply(chunked_keystroke_features)
    test = test.groupby(level=0).apply(keystroke_features)
    
    train, test = normalize(train, test)
    
    # 1 nearest neighbor classifier using Manhattan distance (p=1)
    knn = KNeighborsClassifier(n_neighbors=1, p=1)
    knn.fit(train.values, train.index.get_level_values(0))
    result = knn.predict(test.values)
    
    # Save the results
    pd.DataFrame(result, index=test.index, columns=['user']).to_csv(fname_result)